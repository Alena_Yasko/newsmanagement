package com.yaskoem.newsmanagement.controller;

import com.yaskoem.newsmanagement.command.Command;
import com.yaskoem.newsmanagement.command.CommandFactory;
import com.yaskoem.newsmanagement.command.CommandName;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Controller retrieves all client's requests and delegates them to a proper <tt>Command</tt> object.
 * @author Alena Yasko
 */
public class Controller extends HttpServlet {

    private static final String COMMAND_PARAMETER = "command";

    private static final String COMMAND_FACTORY_BEAN_NAME = "commandFactoryBean";

    private CommandFactory commandFactory;
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(
            this.getServletContext());

        commandFactory = (CommandFactory)context.getBean(COMMAND_FACTORY_BEAN_NAME);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

	/**
     * Delegates request to command.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String commandParameter = request.getParameter(COMMAND_PARAMETER);
        CommandName commandName = CommandName.valueOf(commandParameter);
        Command command = commandFactory.getCommand(commandName);

        command.execute(request, response);
    }
}