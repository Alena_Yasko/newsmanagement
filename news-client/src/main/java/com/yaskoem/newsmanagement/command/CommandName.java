package com.yaskoem.newsmanagement.command;

/**
 * All avialable client's commands to controller. CommandName is used as a map key in <tt>CommandFactory</tt>.
 * @see CommandFactory
 * @author Alena Yasko
 */
public enum CommandName {

    VIEW_NEWS_LIST,

    VIEW_NEWS,

    ADD_COMMENT,

    RESET_FILTER,

    FILTER_NEWS
}
