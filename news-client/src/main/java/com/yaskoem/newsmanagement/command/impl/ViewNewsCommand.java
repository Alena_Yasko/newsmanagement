package com.yaskoem.newsmanagement.command.impl;

import com.yaskoem.newsmanagement.command.Command;
import com.yaskoem.newsmanagement.domain.NewsTO;
import com.yaskoem.newsmanagement.domain.SearchCriteria;
import com.yaskoem.newsmanagement.service.CommonFacadeService;
import com.yaskoem.newsmanagement.util.Page;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * ViewNewsCommand get news with the specified id and news-related info.
 * @see Command
 * @author Alena Yasko
 */
public class ViewNewsCommand implements Command {

	private static final String NEWS_ATTRIBUTE = "newsTO";

	private static final String NEWS_ID_PARAMETER = "newsId";

	private static final String NEIGHBOUR_PARAMETER = "neighbour";

	private static final String SEARCH_CRITERIA_ATTRIBUTE = "searchCriteria";

	private enum Neighbour {PREV, NEXT}

	@Autowired
	private CommonFacadeService commonFacadeService;

	/**
	 * Processes request for viewing news info.
	 * @param request must contain valid news id.
	 */
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		Long newsId = Long.parseLong(request.getParameter(NEWS_ID_PARAMETER));
		NewsTO newsTO = null;

		if (request.getParameter(NEIGHBOUR_PARAMETER) != null) {
			Neighbour neighbour = Neighbour.valueOf(request.getParameter(NEIGHBOUR_PARAMETER));

			SearchCriteria searchCriteria =
				(SearchCriteria) request.getSession().getAttribute(SEARCH_CRITERIA_ATTRIBUTE);
			searchCriteria = searchCriteria == null ? new SearchCriteria() : searchCriteria;

			newsTO = getNeighboringNews(newsId, searchCriteria, neighbour);
		}
		if (request.getParameter(NEIGHBOUR_PARAMETER) == null || newsTO == null) {
			newsTO = commonFacadeService.getNewsById(newsId);
		}
		request.setAttribute(NEWS_ATTRIBUTE, newsTO);

		request.getRequestDispatcher(Page.NEWS.getURL()).forward(request, response);
	}

	private NewsTO getNeighboringNews(long newsId, SearchCriteria searchCriteria, Neighbour neighbour) {
		return neighbour == Neighbour.PREV
		       ? commonFacadeService.getPreviousNews(searchCriteria, newsId)
		       : commonFacadeService.getNextNews(searchCriteria, newsId);
	}
}