package com.yaskoem.newsmanagement.command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Command is used by Controller to be delegated request processing to.
 * @author Alena Yasko
 */
public interface Command {

	/**
     * Processes client's request.
     *
     * @param request delegeted from controller.
     * @param response delegated from controller.
     * @throws ServletException
     * @throws IOException
     * @throws com.yaskoem.newsmanagement.exception.ServiceException
     */
    void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
}
