package com.yaskoem.newsmanagement.command.impl;

import com.yaskoem.newsmanagement.command.Command;
import com.yaskoem.newsmanagement.domain.Author;
import com.yaskoem.newsmanagement.domain.NewsTO;
import com.yaskoem.newsmanagement.domain.SearchCriteria;
import com.yaskoem.newsmanagement.domain.Tag;
import com.yaskoem.newsmanagement.service.CommonFacadeService;
import com.yaskoem.newsmanagement.util.Page;
import com.yaskoem.newsmanagement.util.PagesInfo;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * ViewNewsListCommand gets a specified range of news that meet the requirements of search criteria.
 * @see Command
 * @author Alena Yasko
 */
public class ViewNewsListCommand implements Command {

    private static final String NEWS_LIST_ATTRIBUTE = "newsList";

    private static final String AUTHORS_ATTRIBUTE = "authors";

    private static final String TAGS_ATTRIBUTE = "tags";

    private static final String CURRENT_PAGE_ATTRIBUTE = "currentPage";

    private static final String PAGES_INFO_ATTRIBUTE = "pagesInfo";

    private static final String SEARCH_CRITERIA_ATTRIBUTE = "searchCriteria";

    @Autowired
    private CommonFacadeService commonFacadeService;

    /**
     * Processes request for viewing news list.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {

        HttpSession session = request.getSession();

        SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute(SEARCH_CRITERIA_ATTRIBUTE);
        searchCriteria = searchCriteria == null ? new SearchCriteria() : searchCriteria;

        long numberOfNews = commonFacadeService.countNews(searchCriteria);
        int numberOfPages = (int) Math.ceil((double) numberOfNews / PagesInfo.NEWS_PER_PAGE);

        PagesInfo pagesInfo = (PagesInfo) session.getAttribute(PAGES_INFO_ATTRIBUTE);
        if (pagesInfo == null) {
            pagesInfo = new PagesInfo(numberOfPages);
        }
        else {
            Integer currentPage = request.getParameter(CURRENT_PAGE_ATTRIBUTE) == null
                  ? 1
                  : Integer.valueOf(request.getParameter(CURRENT_PAGE_ATTRIBUTE));

            pagesInfo.update(currentPage, numberOfPages);
        }

        int firstNewsNumber = (pagesInfo.getCurrentPage() - 1) * PagesInfo.NEWS_PER_PAGE + 1;
        List<NewsTO> newsList = commonFacadeService.getNewsList(searchCriteria, firstNewsNumber, PagesInfo.NEWS_PER_PAGE);
        List<Author> authors = commonFacadeService.getNotExpiredAuthors();
        List<Tag> tags = commonFacadeService.getTagsList();

        session.setAttribute(PAGES_INFO_ATTRIBUTE, pagesInfo);
        request.setAttribute(NEWS_LIST_ATTRIBUTE, newsList);
        request.setAttribute(AUTHORS_ATTRIBUTE, authors);
        request.setAttribute(TAGS_ATTRIBUTE, tags);

        request.getRequestDispatcher(Page.NEWS_LIST.getURL()).forward(request, response);
    }
}