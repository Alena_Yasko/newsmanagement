package com.yaskoem.newsmanagement.command.impl;

import com.yaskoem.newsmanagement.command.Command;
import com.yaskoem.newsmanagement.domain.Comment;
import com.yaskoem.newsmanagement.service.CommonFacadeService;
import com.yaskoem.newsmanagement.Validator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

/**
 * AddCommentCommand adds client's comment to news.
 * @see Command
 * @author Alena Yasko
 */
public class AddCommentCommand implements Command {

	private static final String NEWS_ID_PARAMETER = "newsId";

	private static final String COMMENT_TEXT_PARAMETER = "commentText";

	@Autowired
	private CommonFacadeService commonFacadeService;

	/**
	 * Processes request for adding comment.
	 * @param request should contain news id and comment text.
	 */
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {

		long newsId = Long.parseLong(request.getParameter(NEWS_ID_PARAMETER));
		String commentText = request.getParameter(COMMENT_TEXT_PARAMETER);

		if (Validator.isCommentValid(commentText)) {
			Comment comment = new Comment(newsId, commentText);
			commonFacadeService.addComment(comment);
		}

		String forwardUrl = String.format("/controller?command=VIEW_NEWS&newsId=%d", newsId);
		request.getRequestDispatcher(forwardUrl).forward(request, response);
	}
}