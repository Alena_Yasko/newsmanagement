package com.yaskoem.newsmanagement.command.impl;

import com.yaskoem.newsmanagement.command.Command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * ResetFilterCommand resets news filter.
 * @see Command
 * @author Alena Yasko
 */
public class ResetFilterCommand implements Command {

    private static final String PAGES_INFO_SESSION_ATTRIBUTE = "pagesInfo";

    private static final String SEARCH_CRITERIA_SESSION_ATTRIBUTE = "searchCriteria";

    /**
     * Processes request for resetting filter. Clears searching criteria and pages info
     * and delegates request to <tt>ViewNewsListCommand</tt>.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        session.removeAttribute(PAGES_INFO_SESSION_ATTRIBUTE);
        session.removeAttribute(SEARCH_CRITERIA_SESSION_ATTRIBUTE);

        request.getRequestDispatcher("/controller?command=VIEW_NEWS_LIST").forward(request, response);
    }
}
