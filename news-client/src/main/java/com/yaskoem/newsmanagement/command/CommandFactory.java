package com.yaskoem.newsmanagement.command;

import com.yaskoem.newsmanagement.command.impl.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.HashMap;
import java.util.Map;

/**
 * A helper class used by controller to get <tt>Command</tt> object by <tt>CommandName</tt>.
 * @author Alena Yasko
 */
public class CommandFactory {

    private Map<CommandName, Command> commands;

    @Autowired
    public CommandFactory(AddCommentCommand addCommentCommand, ViewNewsCommand viewNewsCommand,
                          ViewNewsListCommand viewNewsListCommand, FilterNewsCommand filterNewsCommand,
                          ResetFilterCommand resetFilterCommand) {

        commands = new HashMap<>();

        commands.put(CommandName.VIEW_NEWS_LIST, viewNewsListCommand);
        commands.put(CommandName.VIEW_NEWS, viewNewsCommand);
        commands.put(CommandName.ADD_COMMENT, addCommentCommand);
        commands.put(CommandName.FILTER_NEWS, filterNewsCommand);
        commands.put(CommandName.RESET_FILTER, resetFilterCommand);
    }

	/**
     * Returns a Command object by the specified command name.
     * @param commandName
     * @return
     */
    public Command getCommand(CommandName commandName) {
        return commands.get(commandName);
    }
}