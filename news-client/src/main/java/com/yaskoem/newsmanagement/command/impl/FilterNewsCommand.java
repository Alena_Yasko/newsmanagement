package com.yaskoem.newsmanagement.command.impl;

import com.yaskoem.newsmanagement.command.Command;
import com.yaskoem.newsmanagement.domain.SearchCriteria;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * FilterNewsCommand gets a specified range of news that meet the requirements of search criteria.
 * @see Command
 * @author Alena Yasko
 */
public class FilterNewsCommand implements Command {

	private static final String AUTHOR_ID_PARAMETER = "filterAuthor";

	private static final String TAGS_IDS_PARAMETER = "filterTags";

	private static final String PAGES_INFO_SESSION_ATTRIBUTE = "pagesInfo";

	private static final String SEARCH_CRITERIA_SESSION_ATTRIBUTE = "searchCriteria";

	/**
	 * Processes request for filtering news. Sets searching criteria and resets pages info
	 * and delegates request to <tt>ViewNewsListCommand</tt>.
	 * @param request should contain search criteria and pages info to get the news range.
	 */
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {

		HttpSession session = request.getSession();
		session.removeAttribute(PAGES_INFO_SESSION_ATTRIBUTE);

		List<Long> tagsIds = request.getParameterValues(TAGS_IDS_PARAMETER) != null
             ? Arrays.asList(request.getParameterValues(TAGS_IDS_PARAMETER)).stream()
                   .map(Long::valueOf)
                   .collect(Collectors.toList())
             : null;

		Long authorId = Long.valueOf(request.getParameter(AUTHOR_ID_PARAMETER));

		SearchCriteria searchCriteria = authorId != 0
		    ? new SearchCriteria(authorId, tagsIds)
            : new SearchCriteria(tagsIds);

		session.setAttribute(SEARCH_CRITERIA_SESSION_ATTRIBUTE, searchCriteria);

		request.getRequestDispatcher("/controller?command=VIEW_NEWS_LIST").forward(request, response);
	}
}