package com.yaskoem.newsmanagement.util;

/**
 * Page is used to get pages URLs when forwarding from Command instances.
 * @author Alena Yasko
 */
public enum Page {

    NEWS_LIST("newsList"),

    NEWS("news"),

    ERROR("error"), ;

    private String tileName;

    private static final String TEMPLATE_URL = "/jsp/pageTemplate.jsp?tileName=";

	/**
     * @param tileName name of apache tile to load on template page.
     */
    Page(String tileName) {
        this.tileName = tileName;
    }

    public String getURL() { return TEMPLATE_URL + tileName; }
}