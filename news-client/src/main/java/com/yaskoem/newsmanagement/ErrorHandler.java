package com.yaskoem.newsmanagement;

import com.yaskoem.newsmanagement.util.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class receives all exceptions to log them and show an error page.
 * @author Alena Yasko
 */
public class ErrorHandler extends HttpServlet {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandler.class);
    private static final String EXCEPTION_ATTRIBUTE = "exception";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Throwable exception = (Throwable) request.getAttribute(RequestDispatcher.ERROR_EXCEPTION);

        LOGGER.error("", exception);

        request.setAttribute(EXCEPTION_ATTRIBUTE, exception);

        String forwardPathUri = Page.ERROR.getURL();
        request.getRequestDispatcher(forwardPathUri).forward(request, response);
    }
}