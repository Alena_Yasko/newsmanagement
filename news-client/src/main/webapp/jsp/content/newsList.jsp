<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div class="news-list-div">
    <form class="news-filter" action="${pageContext.request.contextPath}/controller" method="POST">
        <input type="hidden" name="command" value="FILTER_NEWS"/>
        <ul>
            <li><select name="filterAuthor">
                <option value="0" ${searchCriteria.authorId == null ? 'selected' : ''} >Select author</option>
                <c:forEach var="author" items="${authors}">
                    <option value="${author.id}" ${searchCriteria.authorId.equals(author.id) ? "selected" : ""}>
                            ${author.name}
                    </option>
                </c:forEach>
            </select>
            </li>

            <li class="tagsDropdown">

                <ul>
                    <li>Select tags
                        <ul><c:forEach var="tag" items="${tags}">
                            <li><input type="checkbox" name="filterTags" value="${tag.id}" class="filterTags"
                                ${searchCriteria.tagsIds.contains(tag.id) ? "checked" : ""}>
                                    ${tag.name}
                            </li>
                        </c:forEach>
                        </ul>
                    </li>
                </ul>
            </li>

            <li><input type="submit" value="Filter"/></li>

            <li><input type="button" id="resetButton" value="Reset"
                       onclick="window.location.href='${pageContext.request.contextPath}/controller?command=RESET_FILTER'"/>
            </li>
        </ul>
    </form>

    <div class="news-list">
        <ul>
            <c:forEach var="newsTO" items="${newsList}">
                <li>
                    <ul class="news-head-info">
                        <li class="news-title">${newsTO.news.title}</li>

                        <li>(by ${newsTO.author.name})</li>

                        <li class="news-date"><fmt:formatDate value="${newsTO.news.creationDate}"
                                                              pattern="MM/dd/yyyy"/></li>
                    </ul>

                    <div>${newsTO.news.shortText}</div>

                    <ul class="news-additional-info">
                        <li>
                            <ul>
                                <c:forEach var="tag" items="${newsTO.tags}">
                                    <li class="news-tags">${tag.name}</li>
                                </c:forEach>
                            </ul>
                        </li>

                        <li class="comments-count">
                            Comments(${newsTO.comments.size()})
                        </li>

                        <li>
                            <a href="${pageContext.request.contextPath}/controller?command=VIEW_NEWS&newsId=${newsTO.news.id}">
                                View</a>
                        </li>
                    </ul>
                </li>
            </c:forEach>
        </ul>
    </div>

    <div class="pagination">
        <ul>

            <c:if test="${ pagesInfo.firstPageInGroup > 1}">
                <li><a
                        href="${pageContext.request.contextPath}/controller?command=VIEW_NEWS_LIST&currentPage=${pagesInfo.firstPageInGroup - 1}">
                    &lt&lt</a></li>
            </c:if>

            <c:forEach begin="${pagesInfo.firstPageInGroup}" end="${pagesInfo.lastPageInGroup}" varStatus="loop">
                <li><input ${loop.index == pagesInfo.currentPage ? "id='currentPageButton'" : ""} type="button"
                  onclick="window.location.href='${pageContext.request.contextPath}/controller?command=VIEW_NEWS_LIST&currentPage=${loop.index}'"
                                                                                                  value="${loop.index}"/>
                </li>
            </c:forEach>

            <c:if test="${ pagesInfo.lastPageInGroup < pagesInfo.numberOfPages }">
                <li>
                    <a href="${pageContext.request.contextPath}/controller?command=VIEW_NEWS_LIST&currentPage=${pagesInfo.lastPageInGroup + 1}">
                        &gt&gt</a></li>
            </c:if>
        </ul>
    </div>
</div>