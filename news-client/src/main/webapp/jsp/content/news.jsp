<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div class="news">
    <div><a href="${pageContext.request.contextPath}/controller?command=VIEW_NEWS_LIST">BACK</a></div>

    <table class="news-info">
        <tr class="header">
            <td class="news-title">${newsTO.news.title}</td>

            <td class="news-author">(by ${newsTO.author.name})</td>

            <td class="news-date"><fmt:formatDate value="${newsTO.news.modificationDate}" pattern="MM/dd/yyyy"/></td>
        </tr>

        <tr class="news-full">
            <td colspan="3">${newsTO.news.fullText}</td>
        </tr>
    </table>

    <div class="news-comments">
       <ul><c:forEach var="comment" items="${newsTO.comments}">
            <li>
                <div class="comment-date">
                    <fmt:formatDate value="${comment.creationDate}" pattern="MM/dd/yyyy"/>
                </div>
                <div class="comment-text">${comment.text}</div>
            </li>
            </c:forEach>
       </ul>
    </div>

    <form name="post-comment-form" class="post-comment-form" method="post"
          action="${pageContext.request.contextPath}/controller">
        <input type="hidden" name="command" value="ADD_COMMENT" />

        <input type="hidden" name="newsId" value="${newsTO.news.id}" />
        <ul>
            <li><textarea name="commentText" class="not-empty"></textarea></li>

            <li><input type="submit" value="Post comment" /></li>
        </ul>
    </form>

    <div class="navigation">
        <ul>
            <li><a href="${pageContext.request.contextPath}/controller?command=VIEW_NEWS&newsId=${newsTO.news.id}&neighbour=PREV">
            	PREVIOUS</a>
            </li>
            <li><a href="${pageContext.request.contextPath}/controller?command=VIEW_NEWS&newsId=${newsTO.news.id}&neighbour=NEXT">
                NEXT</a>
            </li>
        </ul>
    </div>
</div>