<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<tiles:importAttribute name="stylesheets"/>

<html>
<head>
    <title><tiles:insertAttribute name="title" /></title>

    <c:forEach var="css" items="${stylesheets}">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/styles/${css}" />
    </c:forEach>
</head>

<body>

<header>
    <tiles:insertAttribute name="header" />
</header>

<div class="content">
    <tiles:insertAttribute name="content"/>
</div>

<footer>
    <tiles:insertAttribute name="footer" />
</footer>

</body>

</html>