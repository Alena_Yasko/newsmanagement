package com.yaskoem.newsmanagement.util;

/**
 * TileName is used by controllers.
 * @author Alena Yasko
 */
public enum TileName {

    LOGIN("login"),

    NEWS_LIST("newsList"),

    VIEW_NEWS("viewNews"),

    EDIT_NEWS("editNews"),

    ADD_NEWS("addNews"),

    TAGS("tags"),

    AUTHORS("authors"),

    ERROR("error"),

    ACCESS_DENIED("accessDenied");

    private String value;

    TileName(String value) {
        this.value = value;
    }

    public String getValue() { return value; }
}