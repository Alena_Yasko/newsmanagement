package com.yaskoem.newsmanagement.util;

import java.io.Serializable;

/**
 * PagesInfo is used to get pages information when viewing news list, and is to passed with HttpSession object.
 * @author Alena Yasko
 */
public class PagesInfo implements Serializable {

    /**
     * Specifies number of pages-switching buttons in pagination section of a page.
     */
    public static final int PAGES_GROUP_SIZE = 3;

    public static final int NEWS_PER_PAGE = 4;

    private int currentPage;

    private int numberOfPages;

    /**
     * A number of the first page-switching button in pagination section.
     */
    private int firstPageInGroup;

    /**
     * A number of the last page-switching button in pagination section.
     */
    private int lastPageInGroup;

    public PagesInfo(int numberOfPages) {
        this.firstPageInGroup = 1;
        this.currentPage = 1;
        this.numberOfPages = numberOfPages;
        this.lastPageInGroup = Math.min(PAGES_GROUP_SIZE, numberOfPages);
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public int getFirstPageInGroup() {
        return firstPageInGroup;
    }

    public int getLastPageInGroup() {
        return lastPageInGroup;
    }

    /**
     * Sets all fields to a proper values according to the specified
     * current page and total number of pages.
     */
    public void update(Integer currentPage, int numberOfPages) {
        currentPage = currentPage == null ? 1 : currentPage;

        this.numberOfPages = numberOfPages;

        this.currentPage = Math.min(currentPage, numberOfPages);

        firstPageInGroup =
            ((int) Math.ceil((double) currentPage / PagesInfo.PAGES_GROUP_SIZE) - 1) * PagesInfo.PAGES_GROUP_SIZE + 1;

        lastPageInGroup = Math.min(firstPageInGroup + PagesInfo.PAGES_GROUP_SIZE - 1, numberOfPages);
    }
}