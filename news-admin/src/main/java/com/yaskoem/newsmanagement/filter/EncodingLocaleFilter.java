package com.yaskoem.newsmanagement.filter;

import javax.servlet.*;
import java.io.IOException;
import java.util.Locale;

/**
 * EncodingLocaleFilter sets locale and request/response encodings.
 * @author Alena Yasko
 */
public class EncodingLocaleFilter implements Filter {

    private String encoding;

    private String localeLanguage;

    private String localeCountry;

    /**
     * Gets initial encoding and locale parameters of filter and sets locale.
     */
    @Override
    public void init(FilterConfig fConfig) throws ServletException {
        encoding = fConfig.getInitParameter("encoding");
        localeLanguage = fConfig.getInitParameter("localeLanguage");
        localeCountry = fConfig.getInitParameter("localeCountry");
        Locale.setDefault(new Locale(localeLanguage, localeCountry));
    }

    /**
     * Sets request/response encodings.
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException {

        request.setCharacterEncoding(encoding);
        response.setCharacterEncoding(encoding);

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}