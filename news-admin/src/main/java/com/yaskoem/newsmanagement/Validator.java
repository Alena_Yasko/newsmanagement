package com.yaskoem.newsmanagement;

/**
 * Validator is used to check if client's input is correct.
 * @author Alena Yasko
 */
public class Validator {
	
	public static boolean isCommentValid(String commentText) {
		return (commentText != null && !commentText.trim().isEmpty());
	}
}
