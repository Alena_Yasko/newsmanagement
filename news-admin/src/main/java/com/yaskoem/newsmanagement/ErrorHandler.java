package com.yaskoem.newsmanagement;

import com.yaskoem.newsmanagement.util.TileName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Class receives all exceptions to log them and show an error page.
 * If an exception is an instance of <tt>AccessDeniedException</tt> it
 * is rethrown to be processed by Spring Security.
 *
 * @author Alena Yasko
 */
@ControllerAdvice
public class ErrorHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandler.class);

    @ExceptionHandler(value = Throwable.class)
    public ModelAndView defaultErrorHandler(HttpServletRequest request, Exception exception) throws Exception {
        if (exception instanceof AccessDeniedException) {
            throw exception;
        }

        LOGGER.error("", exception);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("exception", exception);
        modelAndView.addObject("url", request.getRequestURL());
        modelAndView.setViewName(TileName.ERROR.getValue());
        return modelAndView;
    }
}