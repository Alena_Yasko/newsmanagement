package com.yaskoem.newsmanagement.controller;

import com.yaskoem.newsmanagement.domain.Author;
import com.yaskoem.newsmanagement.domain.News;
import com.yaskoem.newsmanagement.domain.NewsTO;
import com.yaskoem.newsmanagement.domain.Role;
import com.yaskoem.newsmanagement.domain.Tag;
import com.yaskoem.newsmanagement.service.AdminFacadeService;
import com.yaskoem.newsmanagement.service.CommonFacadeService;
import com.yaskoem.newsmanagement.util.TileName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * NewsEditController processes requests for changing news.
 * Method calls are performed only for user with <tt>ADMIN</tt> role.
 * @author Alena Yasko
 */
@Secured(Role.ADMIN)
@Controller
@RequestMapping("/news")
public class NewsEditController {

    private static final String NEWS_ATTRIBUTE = "newsTO";

    private static final String AUTHORS_ATTRIBUTE = "authors";

    private static final String TAGS_ATTRIBUTE = "tags";

    private static final String URL_VIEW_NEWS = "forward:/news/viewNews";

    private static final String NEWS_ID_PARAMETER = "newsId";

    @Autowired
    private AdminFacadeService adminFacadeService;

    @Autowired
    private CommonFacadeService commonFacadeService;

    /**
     * Shows an add news page.
     */
    @RequestMapping("/viewAddPage")
    public ModelAndView viewAddPage(ModelAndView modelAndView) {
        modelAndView = setModelAndViewForNewsTO(modelAndView, new NewsTO(new News(), new Author()));
        modelAndView.setViewName(TileName.ADD_NEWS.getValue());

        return modelAndView;
    }

    /**
     * Shows an edit news page with pre-populated fields for a news record with the specified id.
     * The values for filling are passed with newsTO object.
     * @param requestParams should contain id of the news to be edited.
     * @return modelAndView contains newsTO object for fields pre-population.
     */
    @RequestMapping("/viewEditPage")
    public ModelAndView viewEditPage(@RequestParam Map<String, String> requestParams, ModelAndView modelAndView) {
        long newsId = Long.parseLong(requestParams.get(NEWS_ID_PARAMETER));
        NewsTO newsTO = commonFacadeService.getNewsById(newsId);
        prepareFormModelFromTO(newsTO);
        modelAndView = setModelAndViewForNewsTO(modelAndView, newsTO);
        modelAndView.setViewName(TileName.EDIT_NEWS.getValue());

        return modelAndView;
    }

    /**
     * Processes client's request for adding news.
     * @param newsTO an object which fields contain data for a new record in data source.
     */
    @RequestMapping("/addNews")
    public ModelAndView addNews(@ModelAttribute(NEWS_ATTRIBUTE) NewsTO newsTO, ModelAndView modelAndView) {
        completeNewsTOFromFormModel(newsTO);

        long newsId = adminFacadeService.addNews(newsTO);

        modelAndView.setViewName(URL_VIEW_NEWS + "?newsId=" + newsId);

        return modelAndView;
    }

    /**
     * Processes client's request for editing news.
     * @param newsTO an object which fields contain data for the update.
     */
    @RequestMapping("/editNews")
    public ModelAndView editNews(@ModelAttribute(NEWS_ATTRIBUTE) NewsTO newsTO, ModelAndView modelAndView) {
        completeNewsTOFromFormModel(newsTO);

        adminFacadeService.updateNews(newsTO);

        modelAndView.setViewName(URL_VIEW_NEWS + "?newsId=" + newsTO.getNews().getId());

        return modelAndView;
    }

	/**
     * Fills tags list in newsTO with all available tag values to be displayed in spring mvc form element.
     */
    private void prepareFormModelFromTO(NewsTO newsTO) {
        List<Tag> modelTags = new ArrayList<>();
        List<Tag> allTags = commonFacadeService.getTagsList();

        for (Tag tag : newsTO.getTags()) {
            if (allTags.contains(tag)) {
                modelTags.add(tag);
            }
            else {
                tag.setId(null);
                modelTags.add(tag);
            }
        }

        newsTO.setTags(modelTags);
    }

    /**
     * Fills tags list in newsTO with checked values only.
     */
    private NewsTO completeNewsTOFromFormModel(NewsTO newsTO) {
        Author author = adminFacadeService.getAuthor(newsTO.getAuthor().getId());
        newsTO.setAuthor(author);

        List<Tag> tags = newsTO.getTags().stream()
                             .filter(tag -> tag.getId() != null)
                             .map(tag -> adminFacadeService.getTag(tag.getId()))
                             .collect(Collectors.toList());

        newsTO.setTags(tags);

        return newsTO;
    }

	/**
	 * Sets model attributes for tags, authors and news to be displayed on page.
     */
    private ModelAndView setModelAndViewForNewsTO(ModelAndView modelAndView, NewsTO newsTO) {
        List<Tag> tags = commonFacadeService.getTagsList();
        List<Author> authors = commonFacadeService.getNotExpiredAuthors();

        modelAndView.addObject(TAGS_ATTRIBUTE, tags);
        modelAndView.addObject(AUTHORS_ATTRIBUTE, authors);
        modelAndView.addObject(NEWS_ATTRIBUTE, newsTO);

        return modelAndView;
    }
}