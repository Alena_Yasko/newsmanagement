package com.yaskoem.newsmanagement.controller;

import com.yaskoem.newsmanagement.domain.Comment;
import com.yaskoem.newsmanagement.domain.Role;
import com.yaskoem.newsmanagement.service.AdminFacadeService;
import com.yaskoem.newsmanagement.service.CommonFacadeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * CommentsController processes comments-related requests.
 * Method calls are performed only for user with <tt>ADMIN</tt> role.
 * @author Alena Yasko
 */
@Secured(Role.ADMIN)
@Controller
@RequestMapping("/comments")
public class CommentsController {

    private static final String COMMENT_ID_PARAMETER = "id";

    private static final String NEWS_ID_PARAMETER = "newsId";

    private static final String COMMENT_FORM_ATTRIBUTE = "commentForm";

    private static final String URL_VIEW_NEWS = "forward:/news/viewNews";

    @Autowired
    private CommonFacadeService commonFacadeService;

    @Autowired
    private AdminFacadeService adminFacadeService;

    /**
     * Processes client's request for adding a new comment. After adding client is shown the
     * news that the comment was added to.
     * @param commentForm an object which fields contain data for a new record in data source.
     */
    @RequestMapping("/add")
    public ModelAndView addComment(
            @ModelAttribute(COMMENT_FORM_ATTRIBUTE) Comment commentForm, ModelAndView modelAndView) {

        Comment comment = new Comment(commentForm.getNewsId(), commentForm.getText());
        commonFacadeService.addComment(comment);

        modelAndView.addObject(COMMENT_FORM_ATTRIBUTE, new Comment());
        modelAndView.setViewName(URL_VIEW_NEWS + "?newsId=" + commentForm.getNewsId());

        return modelAndView;
    }

    /**
     * Processes client's request for deleting a comment of a specified id.
     * @param requestParams mush contain id of the comment to be deleted.
     */
    @RequestMapping("/delete")
    public ModelAndView deleteComment(@RequestParam Map<String, String> requestParams, ModelAndView modelAndView) {
        long commentId = Long.parseLong(requestParams.get(COMMENT_ID_PARAMETER));

        adminFacadeService.deleteComment(commentId);

        modelAndView.setViewName(URL_VIEW_NEWS + "?newsId=" + requestParams.get(NEWS_ID_PARAMETER));

        return modelAndView;
    }
}