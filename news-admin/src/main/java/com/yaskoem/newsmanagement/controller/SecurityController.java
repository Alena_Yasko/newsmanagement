package com.yaskoem.newsmanagement.controller;

import com.yaskoem.newsmanagement.util.TileName;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * SecurityController manages login/logout.
 *
 * @author Alena Yasko
 */
@Controller
@RequestMapping("/security")
public class SecurityController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/processLogin", method = RequestMethod.POST)
    public String processLogin() {
        return "/news/viewNewsList";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String processLogout() {
        return "login";
    }

    @RequestMapping(value = "/accessDenied", method = RequestMethod.GET)
    public String processAccessDenied() {
        return TileName.ACCESS_DENIED.getValue();
    }
}