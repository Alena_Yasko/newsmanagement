package com.yaskoem.newsmanagement.controller;

import com.yaskoem.newsmanagement.domain.Role;
import com.yaskoem.newsmanagement.domain.Tag;
import com.yaskoem.newsmanagement.service.AdminFacadeService;
import com.yaskoem.newsmanagement.service.CommonFacadeService;
import com.yaskoem.newsmanagement.util.TileName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * TagsController processes tags-related requests.
 * Method calls are performed only for user with <tt>ADMIN</tt> role.
 * @author Alena Yasko
 */
@Secured(Role.ADMIN)
@Controller
@RequestMapping("/tags")
public class TagsController {

    private static final String TAG_ATTRIBUTE = "tag";

    private static final String TAG_ID_ATTRIBUTE = "id";

    private static final String TAGS_ATTRIBUTE = "tags";

    private static final String URL_VIEW_TAGS = "forward:/tags/viewEditPage";

    @Autowired
    private CommonFacadeService commonFacadeService;

    @Autowired
    private AdminFacadeService adminFacadeService;

    /**
     * Shows an edit page with list of all tags. Admin is allowed to add/update/delete tags.
     */
    @RequestMapping("/viewEditPage")
    public ModelAndView viewTags(ModelAndView modelAndView) {
        List<Tag> tags = commonFacadeService.getTagsList();
        modelAndView.addObject(TAGS_ATTRIBUTE, tags);
        modelAndView.addObject(TAG_ATTRIBUTE, new Tag());
        modelAndView.setViewName(TileName.TAGS.getValue());

        return modelAndView;
    }

    /**
     * Processes client's request for deleting a tag of a specified id.
     * @param id id of the tag to be deleted.
     */
    @RequestMapping("/delete")
    public ModelAndView deleteTag(@RequestParam(TAG_ID_ATTRIBUTE) long id, ModelAndView modelAndView) {
        adminFacadeService.deleteTag(id);
        modelAndView.setViewName(URL_VIEW_TAGS);

        return modelAndView;
    }

    /**
     * Processes client's request for editing a tag of a specified id.
     * @param tag an object which fields contain new data for the update.
     */
    @RequestMapping(value = "/edit")
    public ModelAndView editTag(@ModelAttribute(TAG_ATTRIBUTE) Tag tag, ModelAndView modelAndView) {
        adminFacadeService.updateTag(tag);
        modelAndView.setViewName(URL_VIEW_TAGS);

        return modelAndView;
    }

    /**
     * Processes client's request for adding a new tag.
     * @param tag an object which fields contain data for a new record in data source.
     */
    @RequestMapping("/add")
    public ModelAndView addTag(@ModelAttribute("tag") Tag tag, ModelAndView modelAndView) {
        adminFacadeService.addTag(tag);

        modelAndView.setViewName(URL_VIEW_TAGS);

        return modelAndView;
    }
}