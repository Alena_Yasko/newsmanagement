package com.yaskoem.newsmanagement.controller;

import com.yaskoem.newsmanagement.domain.Role;
import com.yaskoem.newsmanagement.domain.Author;
import com.yaskoem.newsmanagement.service.AdminFacadeService;
import com.yaskoem.newsmanagement.service.CommonFacadeService;
import com.yaskoem.newsmanagement.util.TileName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.List;

/**
 * AuthorsController processes authors-related requests.
 * Method calls are performed only for user with <tt>ADMIN</tt> role.
 * @author Alena Yasko
 */
@Secured(Role.ADMIN)
@Controller
@RequestMapping("/authors")
public class AuthorsController {

    private static final String AUTHOR_ATTRIBUTE = "author";

    private static final String AUTHOR_ID_ATTRIBUTE = "id";

    private static final String AUTHORS_ATTRIBUTE = "authors";

    private static final String URL_VIEW_AUTHORS = "forward:/authors/viewEditPage";

    @Autowired
    private CommonFacadeService commonFacadeService;

    @Autowired
    private AdminFacadeService adminFacadeService;

	/**
     * Shows an edit page with list of all non-expired authors. Admin is allowed to add/update/expire authors.
     */
    @RequestMapping("/viewEditPage")
    public ModelAndView viewAuthors(ModelAndView modelAndView) {
        List<Author> authors = commonFacadeService.getNotExpiredAuthors();
        modelAndView.addObject(AUTHORS_ATTRIBUTE, authors);
        modelAndView.addObject(AUTHOR_ATTRIBUTE, new Author());
        modelAndView.setViewName(TileName.AUTHORS.getValue());

        return modelAndView;
    }

    /**
     * Processes client's request for expiring an author of a specified id.
     * @param id id of the author to be made expired.
     */
    @RequestMapping("/setExpired")
    public ModelAndView setAuthorExpired(@RequestParam(AUTHOR_ID_ATTRIBUTE) long id, ModelAndView modelAndView) {
        Author author = adminFacadeService.getAuthor(id);
        author.setExpired(new Date());
        adminFacadeService.updateAuthor(author);
        modelAndView.setViewName(URL_VIEW_AUTHORS);

        return modelAndView;
    }

    /**
     * Processes client's request for editing an author of a specified id.
     * @param author an object which fields contain new data for the update.
     */
    @RequestMapping(value = "/edit")
    public ModelAndView editAuthor(@ModelAttribute(AUTHOR_ATTRIBUTE) Author author, ModelAndView modelAndView) {
        adminFacadeService.updateAuthor(author);
        modelAndView.setViewName(URL_VIEW_AUTHORS);

        return modelAndView;
    }

    /**
     * Processes client's request for adding a new author.
     * @param author an object which fields contain data for a new record in data source.
     */
    @RequestMapping("/add")
    public ModelAndView addAuthor(@ModelAttribute("author") Author author, ModelAndView modelAndView) {
        adminFacadeService.addAuthor(author);

        modelAndView.setViewName(URL_VIEW_AUTHORS);

        return modelAndView;
    }
}