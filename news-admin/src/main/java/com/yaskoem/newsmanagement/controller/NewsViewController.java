package com.yaskoem.newsmanagement.controller;

import com.yaskoem.newsmanagement.domain.*;
import com.yaskoem.newsmanagement.util.*;
import com.yaskoem.newsmanagement.service.CommonFacadeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

import java.util.List;
import java.util.Map;

/**
 * NewsViewController processes requests for viewing news data.
 * Method calls are performed only for user with <tt>ADMIN</tt> role.
 * @author Alena Yasko
 */
@Secured(Role.ADMIN)
@Controller
@RequestMapping("/news")
public class NewsViewController {

    private static final String SEARCH_CRITERIA_ATTRIBUTE = "searchCriteria";

    private static final String PAGES_INFO_ATTRIBUTE = "pagesInfo";

    private static final String CURRENT_PAGE_ATTRIBUTE = "currentPage";

    private static final String NEWS_LIST_ATTRIBUTE = "newsList";

    private static final String NEWS_ATTRIBUTE = "newsTO";

    private static final String AUTHORS_ATTRIBUTE = "authors";

    private static final String TAGS_ATTRIBUTE = "tags";

    private static final String NEWS_ID_PARAMETER = "newsId";

    private static final String NEIGHBOUR_PARAMETER = "neighbour";

    private static final String NEWS_FILTER_ATTRIBUTE = "newsFilter";

    private static final String URL_FORWARD_VIEW_NEWS_LIST = "forward:/news/viewNewsList";

    private static final String COMMENT_ATTRIBUTE = "comment";

    private enum Neighbour {PREV, NEXT}

    @Autowired
    private CommonFacadeService commonFacadeService;

	/**
     * Processes client's request for showing news list for the specified current page and search criteria.
     * @param requestParams should contain <tt>currentPage</tt> parameter.
     * @return modelAndView contains the list of news to be viewed.
     */
    @RequestMapping("/viewNewsList")
    public ModelAndView viewNewsList(
        @RequestParam Map<String, String> requestParams, ModelAndView modelAndView, HttpSession session) {

        SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute(SEARCH_CRITERIA_ATTRIBUTE);
        searchCriteria = searchCriteria == null ? new SearchCriteria() : searchCriteria;

        long numberOfNews = commonFacadeService.countNews(searchCriteria);
        int numberOfPages = (int) Math.ceil((double) numberOfNews / PagesInfo.NEWS_PER_PAGE);

        PagesInfo pagesInfo = (PagesInfo) session.getAttribute(PAGES_INFO_ATTRIBUTE);
        if (pagesInfo == null) {
            pagesInfo = new PagesInfo(numberOfPages);
        }
        else {
            Integer currentPage = requestParams.get(CURRENT_PAGE_ATTRIBUTE) == null
                ? 1
                : Integer.valueOf(requestParams.get(CURRENT_PAGE_ATTRIBUTE));

            pagesInfo.update(currentPage, numberOfPages);
        }

        int firstNewsNumber = (pagesInfo.getCurrentPage() - 1) * PagesInfo.NEWS_PER_PAGE + 1;
        List<NewsTO> newsList = commonFacadeService.getNewsList(searchCriteria, firstNewsNumber, PagesInfo.NEWS_PER_PAGE);
        List<Author> authors = commonFacadeService.getNotExpiredAuthors();
        List<Tag> tags = commonFacadeService.getTagsList();

        session.setAttribute(PAGES_INFO_ATTRIBUTE, pagesInfo);
        modelAndView.addObject(NEWS_LIST_ATTRIBUTE, newsList);
        modelAndView.addObject(AUTHORS_ATTRIBUTE, authors);
        modelAndView.addObject(TAGS_ATTRIBUTE, tags);

        modelAndView.addObject(NEWS_FILTER_ATTRIBUTE, searchCriteria);

        modelAndView.setViewName(TileName.NEWS_LIST.getValue());
        return modelAndView;
    }

    /**
     * Processes client's request for viewing news with.
     * @param requestParams should contain <tt>newsId</tt> parameter.
     *                      <tt>neighbour</tt> parameter is optional and can be <tt>PREV</tt> or <tt>NEXT</tt>;
     *                      if specified, neighbour of news with newsId is shown.
     *
     * @return modelAndView contains newsTO object or <tt>null</tt> if no proper news was found.
     */
    @RequestMapping("/viewNews")
    public ModelAndView viewNews(
        @RequestParam Map<String, String> requestParams, ModelAndView modelAndView, HttpSession session) {

        Long newsId = Long.parseLong(requestParams.get(NEWS_ID_PARAMETER));
        NewsTO newsTO = null;

        if (requestParams.get(NEIGHBOUR_PARAMETER) != null) {
            Neighbour neighbour = Neighbour.valueOf(requestParams.get(NEIGHBOUR_PARAMETER));

            SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute(SEARCH_CRITERIA_ATTRIBUTE);
            searchCriteria = searchCriteria == null ? new SearchCriteria() : searchCriteria;

            newsTO = getNeighboringNews(newsId, searchCriteria, neighbour);
        }
        if (requestParams.get(NEIGHBOUR_PARAMETER) == null || newsTO == null) {
            newsTO = commonFacadeService.getNewsById(newsId);
        }
        modelAndView.addObject(NEWS_ATTRIBUTE, newsTO);
        modelAndView.addObject(COMMENT_ATTRIBUTE, new Comment());
        modelAndView.setViewName(TileName.VIEW_NEWS.getValue());

        return modelAndView;
    }

    /**
     * Processes request for filtering news. Sets searching criteria and resets pages info
     * and delegates request to <tt>viewNewsList</tt>.
     */
    @RequestMapping("/filterNews")
    public ModelAndView filterNews(@ModelAttribute(NEWS_FILTER_ATTRIBUTE) SearchCriteria searchCriteria, HttpSession session) {
        session.removeAttribute(PAGES_INFO_ATTRIBUTE);

        session.setAttribute(SEARCH_CRITERIA_ATTRIBUTE, searchCriteria);

        return new ModelAndView(URL_FORWARD_VIEW_NEWS_LIST);
    }

    /**
     * Processes request for resetting filter. Clears searching criteria and pages info
     * and delegates request to <tt>viewNewsList</tt>.
     */
    @RequestMapping("/resetFilter")
    public ModelAndView resetFilter(HttpSession session) {
        session.removeAttribute(PAGES_INFO_ATTRIBUTE);
        session.removeAttribute(SEARCH_CRITERIA_ATTRIBUTE);

        return new ModelAndView(URL_FORWARD_VIEW_NEWS_LIST);
    }

    private NewsTO getNeighboringNews(long newsId, SearchCriteria searchCriteria, Neighbour neighbour) {
        return neighbour == Neighbour.PREV
            ? commonFacadeService.getPreviousNews(searchCriteria, newsId)
            : commonFacadeService.getNextNews(searchCriteria, newsId);
    }
}