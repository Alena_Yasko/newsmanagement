<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<ul>
    <li><a href="${pageContext.request.contextPath}/news/resetFilter">News List</a></li>

    <li><a href="${pageContext.request.contextPath}/news/viewAddPage">Add news</a></li>

    <li><a href="${pageContext.request.contextPath}/authors/viewEditPage">Add/Update Authors</a></li>

    <li><a href="${pageContext.request.contextPath}/tags/viewEditPage">Add/Update Tags</a></li>
</ul>