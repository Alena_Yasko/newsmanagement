<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<h3><a href="${pageContext.request.contextPath}/security/login"> << BACK TO LOGIN</a></h3>

<div class="message">Access denied! Sorry, this app is for ADMINS only.
</div>