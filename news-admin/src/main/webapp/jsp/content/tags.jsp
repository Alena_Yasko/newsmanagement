<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<script type="text/javascript">
    function enableButton(tagId) {
        var element = document.getElementById("button" + tagId);
        element.removeAttribute("disabled");
    }
</script>

<div class="scrollable-content">
    <ul class="tags-list">
        <c:forEach var="tag" items="${tags}">
            <li class="tag-field">

                <form:form method="post" modelAttribute="tag" action="${pageContext.request.contextPath}/tags/edit">
                    <span>Tag: </span>
                    <form:input type="text" value="${tag.name}" path="name" pattern="[a-zA-Z0-9]{3,30}"
                                onchange='enableButton(${tag.id})'/>

                    <form:input path="id" type="hidden" value="${tag.id}"/>
                    <input type="submit" value="Edit" id="button${tag.id}" disabled/>
                </form:form>
            </li>

            <li>
                <form:form id="deleteForm${tag.id}" method="post"
                           action="${pageContext.request.contextPath}/tags/delete">
                    <input name="id" type="hidden" value="${tag.id}">
                    <input type="submit" value="Delete"/>
                </form:form>
            </li>

        </c:forEach>
    </ul>

    <form:form method="post" action="${pageContext.request.contextPath}/tags/add">
        <span>Add tag: </span>
        <input type="text" name="name" pattern="[a-zA-Z0-9]{3,30}" required/>
        <input type="submit" value="Save"/>
    </form:form>
</div>