<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div class="scrollable-content">
    <div><a href="${pageContext.request.contextPath}/news/viewNewsList?currentPage=${pagesInfo.currentPage}">BACK</a></div>

    <ul class="news-header">
        <li class="edit-link"><a href="${pageContext.request.contextPath}/news/viewEditPage?newsId=${newsTO.news.id}">Edit</a>
        </li>

        <li class="news-title">${newsTO.news.title}</li>

        <li class="news-author">(by ${newsTO.author.name})</li>

        <li class="news-date"><fmt:formatDate value="${newsTO.news.modificationDate}" pattern="MM/dd/yyyy"/></li>
    </ul>

    <div class="news-short">${newsTO.news.shortText}</div>

    <div class="news-full">${newsTO.news.fullText}</div>

    <div class="news-comments">
        <ul><c:forEach var="comment" items="${newsTO.comments}">
            <li>
                <div class="comment-date">
                    <fmt:formatDate value="${comment.creationDate}" pattern="MM/dd/yyyy"/>
                </div>
                <ul>
                    <li class="comment-text">${comment.text}</li>

                    <li class="delete-comment">
                        <button onclick="window.location.href=
                                '${pageContext.request.contextPath}/comments/delete?id=${comment.id}&newsId=${newsTO.news.id}'">
                            X
                        </button>
                    </li>
                </ul>
            </li>
        </c:forEach>
        </ul>
    </div>

    <form:form modelAttribute="comment" name="post-comment-form" class="post-comment-form" method="post"
               action="${pageContext.request.contextPath}/comments/add">
        <form:hidden path="newsId" value="${newsTO.news.id}"/>

        <ul>
            <li><form:textarea path="text" name="commentText" required="required"></form:textarea></li>

            <li><input type="submit" value="Post comment"/></li>
        </ul>
    </form:form>

    <div class="navigation">
        <ul>
            <li><a href="${pageContext.request.contextPath}/news/viewNews?newsId=${newsTO.news.id}&neighbour=PREV">
                PREVIOUS</a>
            </li>
            <li><a href="${pageContext.request.contextPath}/news/viewNews?newsId=${newsTO.news.id}&neighbour=NEXT">
                NEXT</a>
            </li>
        </ul>
    </div>
</div>