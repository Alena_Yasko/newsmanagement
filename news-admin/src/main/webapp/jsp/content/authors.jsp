<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<script type="text/javascript">
    function enableButton(authorId) {
        var element = document.getElementById("button" + authorId);
        element.removeAttribute("disabled");
    }
</script>

<div class="scrollable-content">
    <ul class="authors-list">
        <c:forEach var="author" items="${authors}">
            <li class="author-field">

                <form:form method="post" modelAttribute="author" action="${pageContext.request.contextPath}/authors/edit">
                    <span>Author: </span>
                    <form:input type="text" value="${author.name}" path="name" pattern="[a-zA-Z][a-zA-Z\- ]{2,29}"
                                required="required" onchange='enableButton(${author.id})'/>

                    <form:input path="id" type="hidden" value="${author.id}"/>
                    <input type="submit" value="Edit" id="button${author.id}" disabled/>
                </form:form>
            </li>

            <li>
                <form:form id="expireForm${author.id}" method="post"
                           action="${pageContext.request.contextPath}/authors/setExpired">
                    <input name="id" type="hidden" value="${author.id}">
                    <input type="submit" value="Expire"/>
                </form:form>
            </li>

        </c:forEach>
    </ul>

    <form:form method="post" action="${pageContext.request.contextPath}/authors/add">
        <span>Add author: </span>
        <input type="text" name="name" pattern="[a-zA-Z][a-zA-Z\- ]{2,29}" required/>
        <input type="submit" value="Save"/>
    </form:form>
</div>