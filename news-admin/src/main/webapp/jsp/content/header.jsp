<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<ul>
    <li class="title">
        <h2>News Portal - Administration</h2>
    </li>

    <c:if test="${pageContext.request.userPrincipal.name != null}">

        <li class="greeting">Hello, ${pageContext.request.userPrincipal.name}</li>

        <c:url var="logoutUrl" value="/security/logout"/>

        <li>
            <form id="logoutForm" action="${logoutUrl}" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <input type="submit" value="Logout"/>
            </form>
        </li>
    </c:if>
</ul>