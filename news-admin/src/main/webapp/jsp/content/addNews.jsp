<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="scrollable-content">
    <form:form commandName="newsTO" action="${pageContext.request.contextPath}/news/addNews" method="POST">
        <ul class="fields">
            <li class="news-title">Title: <form:input path="news.title" maxlength="30" required="required"/></li>

            <li class="news-short">Brief <form:textarea path="news.shortText" maxlength="100" required="required"/></li>

            <li class="news-full">Content: <form:textarea path="news.fullText" maxlength="2000" required="required"/></li>

            <li><form:select path="author.id" name="filterAuthor" required="required">
                    <form:options items="${authors}" itemValue="id" itemLabel="name"/>
                </form:select>
            </li>

            <li class="tagsDropdown">
                <ul><li>Tags
                    <ul><c:forEach var="tag" items="${tags}" varStatus="loop">
                        <li><form:checkbox item="${tag}" path="tags['${loop.index}'].id" name="filterTags" value="${tag.id}" class="filterTags" />${tag.name}
                        </li>
                    </c:forEach>
                    </ul></li>
                </ul>
            </li>

            <li><input type="submit" value="Save" /></li>
        </ul>
    </form:form>
</div>