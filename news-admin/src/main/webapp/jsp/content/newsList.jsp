<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div class="news-list-div">
    <form:form commandName="newsFilter" class="news-filter"
               action="${pageContext.request.contextPath}/news/filterNews" method="POST">
        <ul>
            <li>
                <form:select path="authorId" name="filterAuthor">
                    <form:option value="-1" label="Select author"/>
                    <form:options items="${authors}" itemValue="id" itemLabel="name"/>
                </form:select>
            </li>

            <li class="tagsDropdown">

                <ul>
                    <li>Select tags
                        <ul><c:forEach var="tag" items="${tags}">
                            <li><form:checkbox item="${tag}" path="tagsIds" name="filterTags"
                                               value="${tag.id}" class="filterTags"/>${tag.name}
                            </li>
                        </c:forEach>
                        </ul>
                    </li>
                </ul>
            </li>

            <li><input type="submit" value="Filter"/></li>

            <li><input type="button" id="resetButton" value="Reset"
                       onclick="window.location.href='${pageContext.request.contextPath}/news/resetFilter'"/></li>
        </ul>
    </form:form>

    <div class="news-list">
        <ul>
            <c:forEach var="newsTO" items="${newsList}">
                <li>
                    <ul class="news-head-info">
                        <li class="news-title">
                            <a href="${pageContext.request.contextPath}/news/viewNews?newsId=${newsTO.news.id}">
                                    ${newsTO.news.title}
                            </a>
                        </li>

                        <li>(by ${newsTO.author.name})</li>

                        <li class="news-date"><fmt:formatDate value="${newsTO.news.modificationDate}"
                                                              pattern="MM/dd/yyyy"/></li>
                    </ul>

                    <div>${newsTO.news.shortText}</div>

                    <ul class="news-additional-info">
                        <li>
                            <ul>
                                <c:forEach var="tag" items="${newsTO.tags}">
                                    <li class="news-tags">${tag.name}</li>
                                </c:forEach>
                            </ul>
                        </li>

                        <li class="comments-count">
                            Comments(${newsTO.comments.size()})
                        </li>

                        <li>
                            <a href="${pageContext.request.contextPath}/news/viewEditPage?newsId=${newsTO.news.id}">
                                Edit</a>
                        </li>
                    </ul>
                </li>
            </c:forEach>
        </ul>
    </div>

    <div class="pagination">
        <ul>

            <c:if test="${ pagesInfo.firstPageInGroup > 1}">
                <li><a href="${pageContext.request.contextPath}/news/viewNewsList?currentPage=${pagesInfo.firstPageInGroup - 1}">
                    &lt&lt</a></li>
            </c:if>

            <c:forEach begin="${pagesInfo.firstPageInGroup}" end="${pagesInfo.lastPageInGroup}" varStatus="loop">
                <li><input ${loop.index == pagesInfo.currentPage ? "id='currentPageButton'" : ""} type="button"
                                                                                                  onclick="window.location.href='${pageContext.request.contextPath}/news/viewNewsList?currentPage=${loop.index}'"
                                                                                                  value="${loop.index}"/></li>
            </c:forEach>

            <c:if test="${ pagesInfo.lastPageInGroup < pagesInfo.numberOfPages }">
                <li><a href="${pageContext.request.contextPath}/news/viewNewsList?currentPage=${pagesInfo.lastPageInGroup + 1}">
                    &gt&gt</a></li>
            </c:if>
        </ul>
    </div>

</div>