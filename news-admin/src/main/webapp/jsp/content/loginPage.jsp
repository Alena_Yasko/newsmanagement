<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="login">
    <form name="loginForm" action="<c:url value='/security/processLogin'/>" method="POST">

        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

        <c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
            <div class="error">${SPRING_SECURITY_LAST_EXCEPTION.message}</div>
        </c:if>

        <div>
            <span>Login:</span>
            <input type="text" name="username">
        </div>

        <div>
            <span>Password:</span>
            <input type="password" name="password"/>
        </div>

        <input name="submit" type="submit" value="Login"/>
    </form>
</div>