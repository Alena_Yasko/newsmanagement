<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tiles:importAttribute name="scripts"/>
<tiles:importAttribute name="stylesheets"/>

<html>
<head>
    <title><tiles:insertAttribute name="title"/></title>

    <c:forEach var="css" items="${stylesheets}">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/styles/${css}"/>
    </c:forEach>
</head>

<body>

<table>
    <tr>
        <td colspan="2" class="header">
            <tiles:insertAttribute name="header"/>
        </td>
    </tr>

    <tr>
        <td class="menu">
            <tiles:insertAttribute name="menu"/>
        </td>
        <td class="content">
            <tiles:insertAttribute name="content"/>
        </td>
    </tr>
    <tr>
        <td class="footer" colspan="2">
            <tiles:insertAttribute name="footer"/>
        </td>
    </tr>
</table>
</body>

</html>