**NewsManagement project is a news portal.**

The project consists of the following modules:
**1. news-common - backend**

   **Functionality:**
    -add news
    -edit news
    -delete news
    -view the list of news (pagination)
    -view single news item
    -add news author
    -search news according to search criteria (author, tags)
    -add tag(s) for a news item
    -add comment(s) for a news item
    -delete comment(s)
    -sort news by creation date and by most commented news
    -count all news
    -expire author(s)
**
2. news-admin - web UI for administrator**
	Available for the user, logged in as admin.
	
**3. news-client - web UI for any news reader**
	Clients are not logged in.

**Technology stack:**
-Maven build tool
-Mokito and DBUnit for testing
-Spring DI
-Spring MVC (news-admin)
-Spring Security
-Java Servlet (news-client) 
-Apache Tiles
-AspectJ
-Logback

**
Database Installation:**
**1.** Install Oracle Database Express Edition 11g Release 2 (http://www.oracle.com/technetwork/database/database-technologies/express-edition/downloads/index.html)
**2.** Create user "NEWS" with password "password". Use "news-common\src\test\dbscript\ddl\create_db.sql" script to create db, and "news-common\src\test\dbscript\dml\fill_db.sql" to fill it with test data.

**Deployment:**
**1.** Go to the root project folder (newsmanagement) and run "mvn package -DskipTests" in command line to create backend jar and war's for client and admin.
**2.** Deploy "news-client\target\news-client.war" or "news-admin\target\news-admin.war" under Tomcat 8.0.30

**Access UI by URLs "localhost:<port>/news-client" or "localhost:<port>/news-admin**"