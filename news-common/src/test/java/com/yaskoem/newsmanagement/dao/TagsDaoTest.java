package com.yaskoem.newsmanagement.dao;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.yaskoem.newsmanagement.domain.Tag;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.util.List;
import java.util.Locale;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:dataset/test-tags-dataset.xml")
@DatabaseTearDown(value = "classpath:dataset/test-tags-dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class TagsDaoTest {

    static {
        Locale.setDefault(Locale.US);
    }

    @Autowired
    private TagsDao tagsDao;

    @Test
    public void addTest() {
        Tag newTag = new Tag("literature");

        long newTagId = tagsDao.add(newTag);
        newTag.setId(newTagId);

        List<Tag> newAllTags = tagsDao.loadAll();

        Assert.assertTrue(newAllTags.contains(newTag));
    }

    @Test
    public void bindToNewsTest() {
        long newsId = 1L;
        Tag tagToBind = new Tag(3L, "nature");

        List<Tag> oldTagsList = tagsDao.loadByNewsId(newsId);
        for (Tag tag : oldTagsList) {
            Assert.assertFalse(tag.getId().equals(tagToBind.getId()));
        }

        tagsDao.bindToNews(newsId, tagToBind.getId());
        List<Tag> newTagsList = tagsDao.loadByNewsId(newsId);
        Assert.assertTrue(newTagsList.contains(tagToBind));
    }

    @Test
    public void loadTest() {
        Tag tag = new Tag(3L, "nature");

        Tag loadedTag = tagsDao.load(tag.getId());

        Assert.assertEquals(tag, loadedTag);
    }

    @Test
    public void loadByNewsIdTest() {
        long newsId = 1L;
        Tag tag1 = new Tag(1L, "music");
        Tag tag2 = new Tag(2L, "science");

        List<Tag> newsTags = tagsDao.loadByNewsId(newsId);
        Assert.assertTrue(newsTags.size() == 2);
        Assert.assertTrue(newsTags.contains(tag1));
        Assert.assertTrue(newsTags.contains(tag2));
    }

    @Test
    public void loadAllTest() {
        Tag tag1 = new Tag(1L, "music");
        Tag tag2 = new Tag(2L, "science");
        Tag tag3 = new Tag(3L, "nature");

        List<Tag> allTags = tagsDao.loadAll();

        Assert.assertTrue(allTags.size() == 3);
        Assert.assertTrue(allTags.contains(tag1));
        Assert.assertTrue(allTags.contains(tag2));
        Assert.assertTrue(allTags.contains(tag3));
    }

    @Test
    public void unbindFromNewsTest() {
        long newsId = 3L;
        long tagId = 3L;

        List<Tag> newsTags = tagsDao.loadByNewsId(newsId);
        Assert.assertFalse(newsTags.isEmpty());

        tagsDao.unbindFromNews(newsId, tagId);
        List<Tag> updatedNewsTags = tagsDao.loadByNewsId(newsId);
        Assert.assertTrue(updatedNewsTags.isEmpty());
    }

    @Test
    public void unbindFromAllNewsTest() {
        Tag tag = new Tag(1L, "music");

        long newsId1 = 1L;
        long newsId2 = 2L;

        List<Tag> news1Tags = tagsDao.loadByNewsId(newsId1);
        Assert.assertTrue(news1Tags.contains(tag));

        List<Tag> news2Tags = tagsDao.loadByNewsId(newsId2);
        Assert.assertTrue(news2Tags.contains(tag));

        tagsDao.unbindFromAllNews(tag.getId());

        news1Tags = tagsDao.loadByNewsId(newsId1);
        Assert.assertFalse(news1Tags.contains(tag));

        news2Tags = tagsDao.loadByNewsId(newsId2);
        Assert.assertFalse(news2Tags.contains(tag));
    }

    @Test
    public void unbindAllFromNewsTest() {
        long newsId = 1L;

        List<Tag> newsTags = tagsDao.loadByNewsId(newsId);
        Assert.assertFalse(newsTags.isEmpty());

        tagsDao.unbindAllFromNews(newsId);
        newsTags = tagsDao.loadByNewsId(newsId);
        Assert.assertTrue(newsTags.isEmpty());
    }
}