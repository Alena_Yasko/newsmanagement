package com.yaskoem.newsmanagement.dao;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.yaskoem.newsmanagement.domain.Author;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.util.List;
import java.util.Locale;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:dataset/test-authors-dataset.xml")
@DatabaseTearDown(value = "classpath:dataset/test-authors-dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class AuthorsDaoTest {

    static {
        Locale.setDefault(Locale.US);
    }

    @Autowired
    private AuthorsDao authorsDao;

    @Test
    public void addTest() {
        Author author = new Author("Patrick Swayze");

        long authorId = authorsDao.add(author);
        author.setId(authorId);

        Author insertedAuthor = authorsDao.load(authorId);

        Assert.assertEquals(author, insertedAuthor);
    }

    @Test
    public void bindToNewsTest() {
        long newsId = 2L;
        long authorId = 2L;

        Author oldNewsAuthor = authorsDao.loadByNewsId(newsId);
        Assert.assertNull(oldNewsAuthor);
        authorsDao.bindToNews(newsId, authorId);
        Author newNewsAuthor = authorsDao.loadByNewsId(newsId);

        Assert.assertTrue(authorId == newNewsAuthor.getId());
    }

    @Test
    public void unbindFromNewsTest() {
        long newsId = 1L;

        authorsDao.unbindFromNews(newsId);
        Author newNewsAuthor = authorsDao.loadByNewsId(newsId);

        Assert.assertNull(newNewsAuthor);
    }

    @Test
    public void loadTest() {
        long authorId = 2L;

        Author author = new Author(authorId, "James Smith", null);
        Author loadedAuthor = authorsDao.load(authorId);

        Assert.assertEquals(author, loadedAuthor);
    }

    @Test
    public void loadByNewsIdTest() {
        long newsId = 3L;
        Author author = new Author(3L, "Robert Brown", null);

        Author loadedAuthor = authorsDao.loadByNewsId(newsId);

        Assert.assertEquals(author, loadedAuthor);
    }

    @Test
    public void loadNotExpiredTest() {
        int notExpiredCount = 2;
        List<Author> notExpiredAuthors = authorsDao.loadNotExpired();

        Assert.assertTrue(notExpiredCount == notExpiredAuthors.size());

        for (Author author : notExpiredAuthors) {
            Assert.assertNull(author.getExpired());
        }
    }
}