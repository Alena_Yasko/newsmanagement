package com.yaskoem.newsmanagement.service;

import com.yaskoem.newsmanagement.dao.oracle.AuthorsOracleDao;
import com.yaskoem.newsmanagement.dao.oracle.CommentsOracleDao;
import com.yaskoem.newsmanagement.dao.oracle.NewsOracleDao;
import com.yaskoem.newsmanagement.dao.oracle.TagsOracleDao;
import com.yaskoem.newsmanagement.domain.Comment;
import com.yaskoem.newsmanagement.domain.SearchCriteria;
import com.yaskoem.newsmanagement.service.impl.CommonFacadeServiceImpl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-context.xml")
public class CommonFacadeServiceTest {

    @Mock
    private NewsOracleDao newsOracleDaoMock;

    @Mock
    private AuthorsOracleDao authorsOracleDaoMock;

    @Mock
    private TagsOracleDao tagsOracleDaoMock;

    @Mock
    private CommentsOracleDao commentsOracleDaoMock;

    @InjectMocks
    private CommonFacadeServiceImpl commonFacadeServiceImpl;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getNewsListTest() {
        SearchCriteria searchCriteria = new SearchCriteria();
        commonFacadeServiceImpl.getNewsList(searchCriteria, 1, 10);
        verify(newsOracleDaoMock, times(1)).loadList(searchCriteria, 1, 10);
    }

    @Test
    public void addCommentTest() {
        Comment comment = new Comment("Test comment");
        commonFacadeServiceImpl.addComment(comment);
        verify(commentsOracleDaoMock, times(1)).add(comment);
    }

    @Test
    public void countNewsTest() {
        SearchCriteria searchCriteria = new SearchCriteria();
        commonFacadeServiceImpl.countNews(searchCriteria);
        verify(newsOracleDaoMock, times(1)).count(searchCriteria);
    }

    @Test
    public void getTagsListTest() {
        commonFacadeServiceImpl.getTagsList();
        verify(tagsOracleDaoMock, times(1)).loadAll();
    }

    @Test
    public void getNotExpiredAuthorsTest() {
        commonFacadeServiceImpl.getNotExpiredAuthors();
        verify(authorsOracleDaoMock, times(1)).loadNotExpired();
    }
}