package com.yaskoem.newsmanagement.service;

import com.yaskoem.newsmanagement.dao.oracle.AuthorsOracleDao;
import com.yaskoem.newsmanagement.dao.oracle.CommentsOracleDao;
import com.yaskoem.newsmanagement.dao.oracle.NewsOracleDao;
import com.yaskoem.newsmanagement.dao.oracle.TagsOracleDao;
import com.yaskoem.newsmanagement.domain.Author;
import com.yaskoem.newsmanagement.domain.News;
import com.yaskoem.newsmanagement.domain.NewsTO;
import com.yaskoem.newsmanagement.domain.Tag;
import com.yaskoem.newsmanagement.service.impl.AdminFacadeServiceImpl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-context.xml")
public class AdminFacadeServiceTest {

    @Mock
    private NewsOracleDao newsOracleDaoMock;

    @Mock
    private AuthorsOracleDao authorsOracleDaoMock;

    @Mock
    private TagsOracleDao tagsOracleDaoMock;

    @Mock
    private CommentsOracleDao commentsOracleDaoMock;

    @InjectMocks
    private AdminFacadeServiceImpl adminFacadeServiceImpl;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addNewsTest() {
        long newsId = 1L;
        News news = new News(newsId, "DBUnit news title", "Short text", "Full text");
        Author author = new Author(1L, "Patrick Swayze");
        List<Tag> tags = Arrays.asList(new Tag(1L, "literature"), new Tag(2L, "health"));
        NewsTO newsTO = new NewsTO(news, author, tags);

        when(newsOracleDaoMock.add(news)).thenReturn(1L);
        when(tagsOracleDaoMock.loadByNewsId(newsId)).thenReturn(new ArrayList<Tag>());

        adminFacadeServiceImpl.addNews(newsTO);

        verify(newsOracleDaoMock, times(1)).add(news);
        verify(authorsOracleDaoMock, times(1)).bindToNews(newsId, author.getId());
        verify(tagsOracleDaoMock, times(1)).loadByNewsId(newsId);
        verify(tagsOracleDaoMock, never()).unbindFromNews(eq(newsId), anyLong());
        verify(tagsOracleDaoMock, times(2)).bindToNews(eq(newsId), anyLong());
    }

    @Test
    public void updateNewsTest() {
        long newsId = 3L;
        News news = new News(newsId, "DBUnit news title", "Short text", "Full text");
        Author author = new Author(1L, "Patrick Swayze");
        List<Tag> newTags = Arrays.asList(new Tag(1L, "literature"), new Tag(2L, "health"));
        NewsTO newsTO = new NewsTO(news, author, newTags);
        List<Tag> oldTags = Collections.singletonList(new Tag(1L, "literature"));

        when(tagsOracleDaoMock.loadByNewsId(newsId))
            .thenReturn(oldTags);

        adminFacadeServiceImpl.updateNews(newsTO);

        verify(newsOracleDaoMock, times(1)).update(news);
        verify(authorsOracleDaoMock, times(1)).bindToNews(newsId, author.getId());
        verify(tagsOracleDaoMock, times(1)).loadByNewsId(newsId);
        verify(tagsOracleDaoMock, never()).unbindFromNews(eq(newsId), anyLong());
        verify(tagsOracleDaoMock, times(1)).bindToNews(newsId, 2L);
    }

    @Test
    public void deleteNewsTest() {
        long newsId = 1L;

        adminFacadeServiceImpl.deleteNews(newsId);

        verify(authorsOracleDaoMock, times(1)).unbindFromNews(newsId);
        verify(tagsOracleDaoMock, times(1)).unbindAllFromNews(newsId);
        verify(commentsOracleDaoMock, times(1)).deleteAllByNewsId(newsId);
        verify(newsOracleDaoMock, times(1)).delete(newsId);
    }

    @Test
    public void deleteCommentTest() {
        adminFacadeServiceImpl.deleteComment(1L);
        verify(commentsOracleDaoMock, times(1)).delete(1L);
    }

    @Test
    public void addTagTest() {
        Tag tag = new Tag("Test tag");
        adminFacadeServiceImpl.addTag(tag);
        verify(tagsOracleDaoMock, times(1)).add(tag);
    }

    @Test
    public void addAuthorTest$() {
        Author author = new Author("Test author");
        adminFacadeServiceImpl.addAuthor(author);
        verify(authorsOracleDaoMock, times(1)).add(author);
    }
}