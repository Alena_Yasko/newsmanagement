package com.yaskoem.newsmanagement.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Alena Yasko
 */
public class Comment implements Serializable {

    private Long id;

    private Long newsId;

    private String text;

    private Date creationDate;

    public Comment() {
    }

    public Comment(String text) {
        this.text = text;
    }

    public Comment(Long newsId, String text) {
        this(text);
        this.newsId = newsId;
    }
    
    public Comment(Long id, Long newsId, String text, Date creationDate) {
    	this(newsId, text);
    	this.id = id;
    	this.creationDate = creationDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Comment comment = (Comment) obj;

        return id != null ? id.equals(comment.id) : comment.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Comment{" +
            "id=" + id +
            ", newsId=" + newsId +
            ", text='" + text + '\'' +
            ", creationDate=" + creationDate +
            '}';
    }
}