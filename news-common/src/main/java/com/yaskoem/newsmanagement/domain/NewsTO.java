package com.yaskoem.newsmanagement.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Alena Yasko
 */
public class NewsTO implements Serializable {

    private News news;

    private Author author;

    private List<Comment> comments = new ArrayList<>();

    private List<Tag> tags = new ArrayList<>();

    public NewsTO() {
    }

    public NewsTO(News news, Author author) {
        this.news = news;
        this.author = author;
    }

    public NewsTO(News news, Author author, List<Tag> tags) {
        this(news, author);
        this.tags = tags;
    }

    public NewsTO(News news, Author author, List<Tag> tags, List<Comment> comments) {
        this(news, author, tags);
        this.comments = comments;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        NewsTO newsTO = (NewsTO) obj;

        return news != null ? news.equals(newsTO.news) : newsTO.news == null;
    }

    @Override
    public int hashCode() {
        return news != null ? news.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "NewsTO{" +
            "news=" + news +
            ", author=" + author +
            ", comments=" + Arrays.toString(comments.toArray()) +
            ", tags=" + tags +
            '}';
    }
}