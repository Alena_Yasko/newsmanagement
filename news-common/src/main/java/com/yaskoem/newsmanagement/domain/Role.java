package com.yaskoem.newsmanagement.domain;

/**
 * @author Alena Yasko
 */
public interface Role {

    String ADMIN = "ROLE_ADMIN";

    String USER = "ROLE_USER";
}
