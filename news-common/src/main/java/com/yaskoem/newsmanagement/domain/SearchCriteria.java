package com.yaskoem.newsmanagement.domain;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * @author Alena Yasko
 */
public class SearchCriteria implements Serializable {
	
	private Long authorId;
	
	private List<Long> tagsIds;

	public SearchCriteria() {
	}
	
	public SearchCriteria(long authorId) {
		this.authorId = authorId;
	}

	public SearchCriteria(List<Long> tagsIds) {
		this.tagsIds = tagsIds;
	}
	
	public SearchCriteria(long authorId, List<Long> tagsIds) {
		this.authorId = authorId;
		this.tagsIds = tagsIds;
	}
	
	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		if (authorId > 0) {
			this.authorId = authorId;
		}
	}

	public List<Long> getTagsIds() {
		return tagsIds;
	}

	public void setTagsIds(List<Long> tagsIds) {
		this.tagsIds = tagsIds;
	}

	@Override
	public String toString() {
		return "SearchCriteria{" +
			"authorId=" + (authorId != null ? authorId : "null") +
			", tagsIds=" + (tagsIds != null ? Arrays.toString(tagsIds.toArray()) : "null") +
			'}';
	}
}