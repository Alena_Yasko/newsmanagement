package com.yaskoem.newsmanagement.domain;

import java.io.Serializable;
import java.util.Date;

import org.springframework.util.Assert;

/**
 * @author Alena Yasko
 */
public class News implements Serializable {

    private Long id;

    private String title;

    private String shortText;

    private String fullText;

    private Date creationDate;

    private Date modificationDate;

    public News() {
    }

    public News(String title, String shortText, String fullText) {
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
    }
    
    public News(Long id, String title, String shortText, String fullText) {
        this(title, shortText, fullText);

        Assert.notNull(id, "id must not be null!");

    	this.id = id;
    }

    public News(Long id, String title, String shortText, String fullText, Date creationDate, Date modificationDate) {
        this(id, title, shortText, fullText);

        Assert.notNull(creationDate, "creationDate must not be null!");
        Assert.notNull(modificationDate, "modificationDate must not be null!");

    	this.creationDate = creationDate;
        this.modificationDate = modificationDate;
    }
    
    public void setId(long id) {
    	this.id = id;
    }

    public Long getId() {
    	return id;
    }
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        News news = (News) obj;

        return id != null ? id.equals(news.id) : news.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "News{" +
            "id=" + id +
            ", title='" + title + '\'' +
            ", shortText='" + shortText + '\'' +
            ", fullText='" + fullText + '\'' +
            ", creationDate=" + creationDate +
            ", modificationDate=" + modificationDate +
            '}';
    }
}