package com.yaskoem.newsmanagement.dao;

import com.yaskoem.newsmanagement.domain.Author;

import java.util.List;

/**
 * AuthorsDao is used to get/put authors-related data from or into data source.
 * @author Alena Yasko
 */
public interface AuthorsDao {

	/**
     * Adds a new record to authors data source. <tt>id</tt>, <tt>creationDate</tt> and <tt>modificationDate</tt> are
	 * generated automatically.
     *
     * @param author an object that represents a new record to be added.
     * @return id of the inserted record.
     * @throws com.yaskoem.newsmanagement.exception.DaoException
     */
    long add(Author author);

	/**
     * Creates a relation between an author and a news record.
     * If the news is already related to another author DaoException is thrown.
     *
     * @throws com.yaskoem.newsmanagement.exception.DaoException
     */
    void bindToNews(long newsId, long authorId);

	/**
     * Finds a record of relation between news with the specified id and it'd author and removes it.
     *
     * @param newsId <tt>id</tt> of the news that we want to unbind author from.
     * @throws com.yaskoem.newsmanagement.exception.DaoException
     */
    void unbindFromNews(long newsId);

    /**
     * Updates an author with the id equal to parameter's <tt>id</tt> field.
     * New data for the update is retrieved from parameter's fields.
     *
     * @param author Author object containing new values. <tt>id</tt> field should not be null.
     * @throws com.yaskoem.newsmanagement.exception.DaoException
     */
    void update(Author author);

	/**
     * Loads an author from data source.
     *
     * @param authorId id of the author to be loaded.
     * @return loaded author or <tt>null</tt> if the id was not found.
     * @throws com.yaskoem.newsmanagement.exception.DaoException
     */
    Author load(long authorId);

    /**
     * Finds a record of an author related to news with the specified id.
     *
     * @param newsId id of the news related to the requested author.
     * @return loaded author or <tt>null</tt> if no author found.
     * @throws com.yaskoem.newsmanagement.exception.DaoException
     */
    Author loadByNewsId(long newsId);

	/**
     * Retrieves all authors' records who's <tt>expired</tt> field is <tt>null</tt>.
     * @return list of expired authors. If no non-expired authors found an empty list is returned.
     */
    List<Author> loadNotExpired();
}