package com.yaskoem.newsmanagement.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * DaoUtils contains static util methods performing operations common for DAO layer.
 * @author Alena Yasko
 */
public abstract class DaoUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(DaoUtils.class);

	/**
     * Tries to close statement. If an exception occurs it is logged and suppressed.
     * @param statement Statement to be closed.
     */
    public static void closeQuietly(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            }
            catch (SQLException e) {
                LOGGER.error("Can't close statement.", e);
            }
        }
    }

	/**
     * Tries to release the connection by calling <tt>DataSourceUtils.doReleaseConnection</tt>.
     * If an exception occurs it is logged and suppressed.
     * @param connection Connection object to be released.
     * @param dataSource DataSource object that was used to retrieve the connection.
     */
    public static void releaseQuietly(Connection connection, DataSource dataSource) {
        if (connection != null && dataSource != null) {
            try {
                DataSourceUtils.doReleaseConnection(connection, dataSource);
            }
            catch (SQLException e) {
                LOGGER.error("Can't release connection.", e);
            }
        }
    }
}