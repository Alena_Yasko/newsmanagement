package com.yaskoem.newsmanagement.dao.oracle;

import com.yaskoem.newsmanagement.dao.AuthorsDao;
import com.yaskoem.newsmanagement.dao.DaoUtils;
import com.yaskoem.newsmanagement.dao.NewsDao;
import com.yaskoem.newsmanagement.domain.SearchCriteria;
import com.yaskoem.newsmanagement.exception.DaoException;
import com.yaskoem.newsmanagement.domain.News;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * NewsOracleDao operates on NEWS and NEWS_* tables in Oracle DB to get news-related data.
 * @see NewsDao
 * @author Alena Yasko
 */
public class NewsOracleDao implements NewsDao {

    private static final String SQL_INSERT_NEWS = "INSERT INTO news (ns_title, ns_short_text, ns_full_text, " +
        "ns_creation_date, ns_modification_date) VALUES (?, ?, ?, CURRENT_TIMESTAMP, CURRENT_DATE)";

    private static final String SQL_UPDATE_NEWS =
        "UPDATE news SET ns_title = ?, ns_short_text = ?, ns_full_text = ?, ns_modification_date = ? WHERE ns_news_id_pk = ?";

    private static final String SQL_SELECT_NEWS_BY_ID = "SELECT ns_news_id_pk, ns_title, ns_short_text, ns_full_text, "
        + "ns_creation_date, ns_modification_date FROM news WHERE ns_news_id_pk = ?";

    private static final String SQL_DELETE_NEWS = "DELETE FROM news WHERE ns_news_id_pk = ?";

    private DataSource dataSource;

    @Autowired
    public NewsOracleDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public long add(News news) {
        long newsId;

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_INSERT_NEWS, new String[]{"ns_news_id_pk"});

            preparedStatement.setString(1, news.getTitle());
            preparedStatement.setString(2, news.getShortText());
            preparedStatement.setString(3, news.getFullText());
            preparedStatement.execute();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            newsId = resultSet.getLong(1);
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with news=%s", news), e);
        }
        finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }

        return newsId;
    }

    @Override
    public void update(News news) {

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS);

            preparedStatement.setString(1, news.getTitle());
            preparedStatement.setString(2, news.getShortText());
            preparedStatement.setString(3, news.getFullText());
            preparedStatement.setDate(4, new java.sql.Date(news.getModificationDate().getTime()));
            preparedStatement.setLong(5, news.getId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with news=%s", news), e);
        }
        finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }
    }

    @Override
    public News load(long newsId) {
        return selectByQuery(SQL_SELECT_NEWS_BY_ID, newsId);
    }

    @Override
    public List<News> loadList(SearchCriteria searchCriteria, int firstNewsNumber, int maxCount) {
        List<News> newsList = new ArrayList<>();

        String query = SearchingQueryBuilder.buildForRange(searchCriteria);
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(query);

            preparedStatement.setInt(1, firstNewsNumber + maxCount - 1);
            preparedStatement.setInt(2, firstNewsNumber);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                newsList.add(getNewsFromResultSet(resultSet));
            }
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with searchCriteria=%s", searchCriteria), e);
        }
        finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }

        return newsList;
    }

    @Override
    public void delete(long newsId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS);

            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with newsId=%d", newsId), e);
        }
        finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }
    }

    @Override
    public long count(SearchCriteria searchCriteria) {
        long numberOfNews;

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SearchingQueryBuilder.buildForCount(searchCriteria));

            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            numberOfNews = resultSet.getLong(1);
        }
        catch (SQLException e) {
            throw new DaoException(e);
        }
        finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }

        return numberOfNews;
    }

    @Override
    public News loadPrevious(SearchCriteria searchCriteria, long newsId) {
        return selectByQuery(SearchingQueryBuilder.buildForPrevious(searchCriteria), newsId);
    }

    @Override
    public News loadNext(SearchCriteria searchCriteria, long newsId) {
        return selectByQuery(SearchingQueryBuilder.buildForNext(searchCriteria), newsId);
    }

    private News selectByQuery(String query, long newsId) {
        News news = null;

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(query);

            preparedStatement.setLong(1, newsId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                news = getNewsFromResultSet(resultSet);
            }
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with newsId=%d", newsId), e);
        }
        finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }

        return news;
    }

    private News getNewsFromResultSet(ResultSet resultSet) throws SQLException {
        Long id = resultSet.getLong(1);
        String title = resultSet.getString(2);
        String shortText = resultSet.getString(3);
        String fullText = resultSet.getString(4);
        Date creationDate = resultSet.getTimestamp(5);
        Date modificationDate = resultSet.getDate(6);

        return new News(id, title, shortText, fullText, creationDate, modificationDate);
    }
}