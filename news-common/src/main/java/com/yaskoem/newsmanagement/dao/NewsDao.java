package com.yaskoem.newsmanagement.dao;

import com.yaskoem.newsmanagement.domain.SearchCriteria;
import com.yaskoem.newsmanagement.domain.News;

import java.util.List;

/**
 * NewsDao is used to get/put plain news-related data, like text and date, from or into data source.
 * @author Alena Yasko
 */
public interface NewsDao {

    /**
     * Adds a new record to authors data source.
     *
     * @param news an object that represents a new record to be added.
     * @return id of the inserted record.
     * @throws com.yaskoem.newsmanagement.exception.DaoException
     */
    long add(News news);

    /**
     * Updates a news record with the id equal to parameter's <tt>id</tt> field.
     * New data for the update is retrieved from parameter's fields.
     *
     * @param news News object containing new values. <tt>id</tt> field should not be null.
     * @throws com.yaskoem.newsmanagement.exception.DaoException
     */
    void update(News news);

    /**
     * Loads a news record from data source.
     *
     * @param newsId id of the news to be loaded.
     * @return loaded news or <tt>null</tt> if the id was not found.
     * @throws com.yaskoem.newsmanagement.exception.DaoException
     */
    News load(long newsId);

    News loadNext(SearchCriteria searchCriteria, long newsId);

    News loadPrevious(SearchCriteria searchCriteria, long newsId);

    /**
     * Selects <tt>maxCount</tt> or less number of news, that meet the requirements of the
     * <tt>searchCriteria</tt>, starting from <tt>firstNewsNumber</tt> news number in data source's list.
     * @return list of loaded news or an empty list if no required news were found.
     * @throws com.yaskoem.newsmanagement.exception.DaoException
     */
	List<News> loadList(SearchCriteria searchCriteria, int firstNewsNumber, int maxCount);

    void delete(long id);

    long count(SearchCriteria searchCriteria);
}