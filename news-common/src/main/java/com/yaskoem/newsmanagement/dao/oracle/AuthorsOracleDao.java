package com.yaskoem.newsmanagement.dao.oracle;

import com.yaskoem.newsmanagement.dao.AuthorsDao;
import com.yaskoem.newsmanagement.dao.DaoUtils;
import com.yaskoem.newsmanagement.domain.Author;
import com.yaskoem.newsmanagement.exception.DaoException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * AuthorsOracleDao operates on AUTHORS and NEWS_AUTHORS tables in Oracle DB to get data.
 * @see AuthorsDao
 * @author Alena Yasko
 */
public class AuthorsOracleDao implements AuthorsDao {

    private static final String SQL_INSERT_AUTHOR = "INSERT INTO authors(au_author_name_uq) VALUES (?)";

    private static final String SQL_ADD_NEWS_AUTHOR = "INSERT INTO news_authors (na_news_id_fk, na_author_id_fk) VALUES (?, ?)";

    private static final String SQL_DELETE_NEWS_AUTHOR = "DELETE FROM news_authors WHERE na_news_id_fk = ?";

    private static final String SQL_SELECT_NEWS_AUTHOR = "SELECT au_author_id_pk, au_author_name_uq, au_expired FROM authors " +
        "INNER JOIN news_authors ON au_author_id_pk = na_author_id_fk WHERE na_news_id_fk = ?";

    private static final String SQL_SELECT_AUTHOR_BY_ID =
        "SELECT au_author_id_pk, au_author_name_uq, au_expired FROM authors WHERE au_author_id_pk = ?";

    private static final String SQL_SELECT_NOT_EXPIRED_AUTHORS =
        "SELECT au_author_id_pk, au_author_name_uq, au_expired FROM authors WHERE au_expired IS NULL";

    private static final String SQL_UPDATE_AUTHOR =
        "UPDATE authors SET au_author_name_uq = ?, au_expired = ? WHERE au_author_id_pk = ?";

    private DataSource dataSource;

    @Autowired
    public AuthorsOracleDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public long add(Author author) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        long authorId;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_INSERT_AUTHOR, new String[]{"au_author_id_pk"});

            preparedStatement.setString(1, author.getName());
            preparedStatement.execute();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            authorId = resultSet.getLong(1);
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Can't add author '%s'", author), e);
        }
        finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }

        return authorId;
    }

    @Override
    public void update(Author author) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_UPDATE_AUTHOR);
            preparedStatement.setString(1, author.getName());

            if (author.getExpired() != null) {
                preparedStatement.setTimestamp(2, new Timestamp(new Date().getTime()));
            }
            else {
                preparedStatement.setNull(2, Types.TIMESTAMP);
            }

            preparedStatement.setLong(3, author.getId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Can't update author '%s'", author), e);
        }
        finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }
    }

    /**
     * Creates new record in NEWS_AUTHORS which show a relation between news and author.
     * If NEWS_AUTHORS contains a record that has news id equal to newsId, DaoException is thrown,
     * as every news must have only one author.
     *
     * @throws com.yaskoem.newsmanagement.exception.DaoException
     */
    @Override
    public void bindToNews(long newsId, long authorId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_ADD_NEWS_AUTHOR);

            preparedStatement.setLong(1, newsId);
            preparedStatement.setLong(2, authorId);
            preparedStatement.execute();
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Can't bind news '%s' to author '%s'", newsId, authorId), e);
        }
        finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }
    }

    /**
     * Finds a record in NEWS_AUTHORS with news id equal to newsId and removes it.
     * It means removing connection between news with specified id and its author.
     *
     * @param newsId - <tt>id</tt> of the news that we want to unbind author from.
     * @throws com.yaskoem.newsmanagement.exception.DaoException
     */
    @Override
    public void unbindFromNews(long newsId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_AUTHOR);

            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Can't unbind author from news '%s'", newsId), e);
        }
        finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }
    }

    /**
     * Finds a record in NEWS_AUTHORS table by the specified news id, gets an author id from it
     * and then uses it to get an author from AUTHORS.
     *
     * @param newsId - id of an author to be loaded from DB.
     * @return loaded author or <tt>null</tt> if such id is not in AUTHORS.
     * @throws com.yaskoem.newsmanagement.exception.DaoException
     */
    @Override
    public Author loadByNewsId(long newsId) {
        Author author = null;

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_NEWS_AUTHOR);

            preparedStatement.setLong(1, newsId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                author = getAuthorFromResultSet(resultSet);
            }
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Can't load author by newsId='%s'", newsId), e);
        }
        finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }

        return author;
    }

    @Override
    public Author load(long authorId) throws DaoException {
        Author author = null;

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_ID);

            preparedStatement.setLong(1, authorId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                author = getAuthorFromResultSet(resultSet);
            }
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Can't load author with id '%s'", authorId), e);
        }
        finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }

        return author;
    }

    @Override
    public List<Author> loadNotExpired() throws DaoException {
        List<Author> authors = new ArrayList<>();

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_NOT_EXPIRED_AUTHORS);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                authors.add(getAuthorFromResultSet(resultSet));
            }
        }
        catch (SQLException e) {
            throw new DaoException("Can't load not expired authors.", e);
        }
        finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }

        return authors;
    }

    private Author getAuthorFromResultSet(ResultSet resultSet) throws SQLException {
        Long id = resultSet.getLong(1);
        String name = resultSet.getString(2);
        Date expired = resultSet.getTimestamp(3);

        return new Author(id, name, expired);
    }
}