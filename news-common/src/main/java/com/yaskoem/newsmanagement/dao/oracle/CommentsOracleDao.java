package com.yaskoem.newsmanagement.dao.oracle;

import com.yaskoem.newsmanagement.dao.CommentsDao;
import com.yaskoem.newsmanagement.dao.DaoUtils;
import com.yaskoem.newsmanagement.domain.Comment;
import com.yaskoem.newsmanagement.exception.DaoException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * CommentsOracleDao operates on COMMENTS table in Oracle DB to get data.
 * @see CommentsDao
 * @author Alena Yasko
 */
public class CommentsOracleDao implements CommentsDao {

	private static final String SQL_INSERT_COMMENT =
        "INSERT INTO comments (cm_news_id_fk, cm_comment_text, cm_creation_date) VALUES (?, ?, CURRENT_TIMESTAMP)";
	
	private static final String SQL_DELETE_COMMENT = "DELETE FROM comments WHERE cm_comment_id_pk = ?";
	
	private static final String SQL_SELECT_NEWS_COMMENTS =
        "SELECT cm_comment_id_pk, cm_news_id_fk, cm_comment_text, cm_creation_date FROM comments WHERE cm_news_id_fk = ?";
	
	private static final String SQL_DELETE_NEWS_COMMENTS = "DELETE FROM comments WHERE cm_news_id_fk = ?";

    private DataSource dataSource;

    @Autowired
    public CommentsOracleDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Long add(Comment comment) {
        long commentId;

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_INSERT_COMMENT, new String[]{"cm_comment_id_pk"});

            preparedStatement.setLong(1, comment.getNewsId());
            preparedStatement.setString(2, comment.getText());
            preparedStatement.execute();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            commentId = resultSet.getLong(1);
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with comment=%s", comment), e);
        }
        finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }

        return commentId;
    }

    @Override
    public void delete(long commentId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENT);

            preparedStatement.setLong(1, commentId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with commentId=%d", commentId), e);
        }
        finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }
    }

    @Override
    public void deleteAllByNewsId(long newsId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_COMMENTS);

            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with newsId=%d", newsId), e);
        }
        finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }
    }

    @Override
    public List<Comment> loadByNewsId(long newsId) {
        List<Comment> comments = new ArrayList<>();

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_NEWS_COMMENTS);

            preparedStatement.setLong(1, newsId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                comments.add(getCommentFromResultSet(resultSet));
            }
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with newsId=%d", newsId), e);
        }
        finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }

        return comments;
    }

    private Comment getCommentFromResultSet(ResultSet resultSet) throws SQLException {
    	Long commentId = resultSet.getLong(1);
        Long newsId = resultSet.getLong(2);
        String text = resultSet.getString(3);
    	Date creationDate = resultSet.getTimestamp(4);

        return new Comment(commentId, newsId, text, creationDate);
    }
}