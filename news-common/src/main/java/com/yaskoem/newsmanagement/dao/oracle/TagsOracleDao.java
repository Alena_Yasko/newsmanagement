package com.yaskoem.newsmanagement.dao.oracle;

import com.yaskoem.newsmanagement.dao.DaoUtils;
import com.yaskoem.newsmanagement.dao.TagsDao;
import com.yaskoem.newsmanagement.domain.Tag;
import com.yaskoem.newsmanagement.exception.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * TagOracleDao operates on TAGS and NEWS_TAGS tables in Oracle DB to get data.
 * @see TagsDao
 * @author Alena Yasko
 */
public class TagsOracleDao implements TagsDao {

    private static final String SQL_INSERT_TAG = "INSERT INTO tags (tg_tag_name_uq) VALUES(?)";

    private static final String SQL_ADD_NEWS_TAG = "INSERT INTO news_tags (nt_news_id_fk, nt_tag_id_fk) VALUES (?, ?)";

    private static final String SQL_SELECT_BY_ID = "SELECT tg_tag_name_uq FROM tags WHERE tg_tag_id_pk = ?";

    private static final String SQL_SELECT_NEWS_TAGS = "SELECT tg_tag_id_pk, tg_tag_name_uq FROM tags " +
            "INNER JOIN news_tags ON nt_tag_id_fk = tg_tag_id_pk WHERE nt_news_id_fk = ?";

    private static final String SQL_SELECT_ALL_TAGS = "SELECT tg_tag_id_pk, tg_tag_name_uq FROM tags";

    private static final String SQL_DELETE_FROM_NEWS = "DELETE FROM news_tags WHERE nt_news_id_fk = ? AND  nt_tag_id_fk = ?";

    private static final String SQL_DELETE_FROM_ALL_NEWS = "DELETE FROM news_tags WHERE nt_tag_id_fk = ?";

    private static final String SQL_DELETE_NEWS_ALL_TAGS = "DELETE FROM news_tags WHERE nt_news_id_fk = ?";

    private static final String SQL_DELETE_TAG = "DELETE FROM tags WHERE tg_tag_id_pk = ?";

    private static final String SQL_UPDATE_TAG = "UPDATE tags SET tg_tag_name_uq = ? WHERE tg_tag_id_pk = ?";

    private DataSource dataSource;

    @Autowired
    public TagsOracleDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public long add(Tag tag) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        long tagId;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_INSERT_TAG, new String[]{"tg_tag_id_pk"});

            preparedStatement.setString(1, tag.getName());
            preparedStatement.execute();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            tagId = resultSet.getLong(1);
        } catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with tag=%s", tag), e);
        } finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }

        return tagId;
    }

    @Override
    public void bindToNews(long newsId, long tagId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_ADD_NEWS_TAG);

            preparedStatement.setLong(1, newsId);
            preparedStatement.setLong(2, tagId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with newsId=%d and tagId=%d", newsId, tagId), e);
        } finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }
    }

    @Override
    public Tag load(long id) {
        Tag tag = null;

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_BY_ID);

            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                tag = new Tag(id, resultSet.getString(1));
            }
        } catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with id=%d", id), e);
        } finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }

        return tag;
    }

    @Override
    public List<Tag> loadByNewsId(long newsId) {
        List<Tag> tags = new ArrayList<>();

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_NEWS_TAGS);

            preparedStatement.setLong(1, newsId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tags.add(new Tag(resultSet.getLong(1), resultSet.getString(2)));
            }
        } catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with newsId=%d", newsId), e);
        } finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }

        return tags;
    }

    @Override
    public List<Tag> loadAll() {
        List<Tag> tags = new ArrayList<>();

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_TAGS);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tags.add(new Tag(resultSet.getLong(1), resultSet.getString(2)));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }

        return tags;
    }

    @Override
    public void unbindFromNews(long newsId, long tagId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_DELETE_FROM_NEWS);

            preparedStatement.setLong(1, newsId);
            preparedStatement.setLong(2, tagId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with newsId=%d and tagId=%d", newsId, tagId), e);
        } finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }
    }

    @Override
    public void unbindFromAllNews(long tagId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_DELETE_FROM_ALL_NEWS);

            preparedStatement.setLong(1, tagId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with tagId=%d", tagId), e);
        } finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }
    }

    @Override
    public void delete(Long tagId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_DELETE_TAG);
            preparedStatement.setLong(1, tagId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }
    }

    @Override
    public void unbindAllFromNews(long newsId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_ALL_TAGS);

            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with newsId=%d", newsId), e);
        } finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }
    }

    @Override
    public void update(Tag tag) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_UPDATE_TAG);

            preparedStatement.setString(1, tag.getName());
            preparedStatement.setLong(2, tag.getId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with tag=%s", tag), e);
        } finally {
            DaoUtils.closeQuietly(preparedStatement);
            DaoUtils.releaseQuietly(connection, dataSource);
        }
    }
}