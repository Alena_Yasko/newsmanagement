package com.yaskoem.newsmanagement.service.impl;

import com.yaskoem.newsmanagement.dao.AuthorsDao;
import com.yaskoem.newsmanagement.dao.CommentsDao;
import com.yaskoem.newsmanagement.dao.NewsDao;
import com.yaskoem.newsmanagement.dao.TagsDao;
import com.yaskoem.newsmanagement.domain.Author;
import com.yaskoem.newsmanagement.domain.Comment;
import com.yaskoem.newsmanagement.domain.News;
import com.yaskoem.newsmanagement.domain.NewsTO;
import com.yaskoem.newsmanagement.domain.SearchCriteria;
import com.yaskoem.newsmanagement.domain.Tag;
import com.yaskoem.newsmanagement.logging.Loggable;
import com.yaskoem.newsmanagement.service.CommonFacadeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * CommonFacadeServiceImpl calls methods of oracle implementation of DAO layer.
 * @see CommonFacadeService
 *
 * @author Alena Yasko
 */
public class CommonFacadeServiceImpl implements CommonFacadeService {

    private NewsDao newsDao;

    private CommentsDao commentsDao;

    private TagsDao tagsDao;

    private AuthorsDao authorsDao;

    @Autowired
    public CommonFacadeServiceImpl(NewsDao newsDao, CommentsDao commentsDao, TagsDao tagsDao, AuthorsDao authorsDao) {
        this.newsDao = newsDao;
        this.commentsDao = commentsDao;
        this.tagsDao = tagsDao;
        this.authorsDao = authorsDao;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Loggable
    public List<NewsTO> getNewsList(SearchCriteria searchCriteria, int firstNewsNumber, int maxCount) {
        List<News> newsList = newsDao.loadList(searchCriteria, firstNewsNumber, maxCount);

        return newsList.stream()
            .map(this::getNewsTO)
            .collect(Collectors.toList());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Loggable
    public NewsTO getNewsById(long newsId) {
        News news = newsDao.load(newsId);
        return news != null ? getNewsTO(news) : null;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Loggable
    public NewsTO getPreviousNews(SearchCriteria searchCriteria, long newsId) {
        News news = newsDao.loadPrevious(searchCriteria, newsId);
        return news != null ? getNewsTO(news) : null;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Loggable
    public NewsTO getNextNews(SearchCriteria searchCriteria, long newsId) {
        News news = newsDao.loadNext(searchCriteria, newsId);
        return news != null ? getNewsTO(news) : null;
    }

    @Override
    @Loggable
    public void addComment(Comment comment) {
        commentsDao.add(comment);
    }

    @Override
    @Loggable
    public long countNews(SearchCriteria searchCriteria) {
        return newsDao.count(searchCriteria);
    }

    @Override
    @Loggable
    public List<Tag> getTagsList() {
        return tagsDao.loadAll();
    }

    @Override
    @Loggable
    public List<Author> getNotExpiredAuthors() {
        return authorsDao.loadNotExpired();
    }

    private NewsTO getNewsTO(News news) {
        Author author = authorsDao.loadByNewsId(news.getId());
        List<Tag> tags = tagsDao.loadByNewsId(news.getId());
        List<Comment> comments = commentsDao.loadByNewsId(news.getId());

        return new NewsTO(news, author, tags, comments);
    }
}